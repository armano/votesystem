MaNGOS Vote System (MVS)

Vote System for people using MaNGOS.
The System is written in PHP script. Works with PHP5.
Supported languages: English, Bulgarian, German.

SVN: http://my-svn.assembla.com/svn/mangos_vote_system/
Log: http://my-trac.assembla.com/mangos_vote_system/log
Timeline: http://my-trac.assembla.com/mangos_vote_system/timeline
Reports (Tickets): http://my-trac.assembla.com/mangos_vote_system/report

Official project forum: http://mangos.osh.nu/forums/index.php?showforum=78

Project Leader and Bulgarian Translation - SUPERGADGET
German Translation - Mythos
Spanish Translation - wbeando
Portuguese Translation - Bruno
Swedish Translation - Furt82 and Jacob
French traslation - Clement DENIS
Russian translation - SparKer
