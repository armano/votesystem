<?php
define("Vote", 1);
session_start();
require "config.php";
require "language.php";
require "functions.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $language["title"]," ",$server_name ?></title>
<style type="text/css">
html, body {
height: 100%;
margin: 0;
padding: 0;
background-color: #000000;
text-align: center;
}
img#bg {
position:absolute;
top:0;
left:0;
width:100%;
}
#content {
position:relative;
z-index:1;
}
body,td,th,h1,a{
color: #FFFFFF;
}
</style>
</head>
<body>
<img src="lich_king.jpg" id="bg" alt="background image" />
<div id="content">
<script type="text/javascript">
function back()
{
  document.location.href="<?php echo $site_link ?>";
}
</script>
<table width="100%" border="0">
	<tr>
		<td align="left">
			<form method="post" action="<?php echo $_SERVER["REQUEST_URI"] ?>">
				<select name="setlang" onchange="this.form.submit();">
				<?php
				foreach($langs as $short => $alang)
				{
					$short == $set_lang ? $selected = " selected =\"selected\"" : $selected = "";
					echo "<option",$selected," value=\"",$short,"\">",$alang[0],"</option>";
				}
				?>
				</select>
			</form>
		</td>
		<td align="right">
<?php
if(isset($_POST["logout"]))
{
	session_destroy();
	$_SESSION = array();
}
if(isset($_SESSION["logged_voting"]))
{
?>
<form method="post" action="index.php">
	<input type="submit" name="logout" value="<?php echo $language["logout"] ?>" />
</form>
<?php
}
else
{
?>
<input type="button" onclick="back()" value="<?php echo $language["back_to_site"] ?>" />
<?php
}
?>
		</td>
	</tr>
</table>
<h1><?php echo $language["title"]," ",$server_name ?></h1>
<h3><?php echo $language["created_by"] ?> <a rel="nofollow" href="mailto:&#115;&#117;&#112;&#101;&#114;&#103;&#97;&#100;&#103;&#101;&#116;&#64;&#97;&#98;&#118;&#46;&#98;&#103;">SUPERGADGET</a></h3>
<br />
<?php
if(!isset($_SESSION["logged_voting"]))
	require "login.php";
else
	require "main.php";
?>
</div>
</body>
</html>
