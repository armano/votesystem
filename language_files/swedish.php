<?php
$language = array(
"title" => "Röstnings system för",
"created_by" => "Gjort av",

"wrong" => "Fel användarnamn och/eller lösenord",
"username" => "Användarnamn:",
"password" => "Lösenord:",
"realm_name" => "Server namn:",
"submit" => "Logga in",
"reset" => "Återställ",
"enter_name_and_pass" => "Fyll i ditt användarnamn och lösenord.",
"not_entered_username" => "Ej skrivit ditt Användarnamn!",
"not_entered_password" => "Ej skrivit ditt lösenord!",

"logout" => "Logga ut",
"realm" => "Server",
"ip" => "IP",
"cur_acc" => "Nuvarande Konto",
"points_1" => "Poäng",
"acc_points_today" => "Konto poäng idag",
"limit" => "Gräns",
"not_voted_yet" => "Du har inte röstat ännu",
"last_vote" => "Din förra röst var före",
"voting_period" => "Du kan rösta en gång varje",
"show_vote" => "Röstnings panel",
"show_reward" => "Belönings panel",
"sites_reset_after" => "Röstningen kommer återställas om",
"choose_char_for_reward" => "Välj karaktär för belöning",
"available_chars" => "Tillgängliga karaktärer",
"cur_char" => "Nuvarande karaktär",

"days" => "dagar",
"hours" => "timmar",
"minutes_and" => "minuter och",
"seconds" => "sekunder",
"chose_site" => "Välj sida för att rösta",
"voting_sites" => "Röstnings sidor",
"voted" => "Röstat",
"yes" => "Ja",
"no" => "Nej",
"status" => "Status",
"online" => "Inloggad",
"offline" => "Ej inloggad",
"available_rewards" => "Tillgängliga belöningar",
"points_2" => "poäng",
"choose" => "Välj",
"was_given" => "Den gavs",
"to" => "till",

"vote_limit_reached" => "Du kan inte rösta något mera idag.",
"vote_tomorrow" => "Du kan rösta imorgon.",

"mail_subject" => "Från röstnings systemet",
"mail_message" => "Tack för din(a) röst/röster!",

"back_to_site" => "Tillbaka till hemsidan",
);
?>