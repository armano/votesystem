<?php
// id => item_id(put 0 for money reward), count, price(vote points), quality(put -1 to disable item tooltip)
$tab_rewards = array(
	1	=> array(0,350000,7,-1),
	2	=> array(0,1000000,14,-1),
	3	=> array(0,4500000,35,-1),
	4	=> array(0,10000000,70,-1),
	5	=> array(20558,1,7,2),
	6	=> array(20559,1,7,2),
	7	=> array(20560,1,7,2),
	8	=> array(29024,1,7,2),
	9	=> array(43589,1,7,2),
	10	=> array(29434,1,7,4),
	11	=> array(40752,1,14,4),
	12	=> array(40753,1,21,4),
	13	=> array(45624,1,28,4),
	14	=> array(34664,1,21,3),
);
// reward_text_language: 0 - English, 1 - Bulgarian, 2 - German, 3 - Spanish,
// 4 - Portuguese, 5 - Swedish, 6 - French, 7 - Russian
// $reward_texts[reward_text_language]
// English
$reward_texts[0] = array(
	1	=> "35 gold",
	2	=> "100 gold",
	3	=> "450 gold",
	4	=> "1000 gold",
	5	=> "Warsong Gulch Mark of Honor",
	6	=> "Arathi Basin Mark of Honor",
	7	=> "Alterac Valley Mark of Honor",
	8	=> "Eye of the Storm Mark of Honor",
	9	=> "Wintergrasp Mark of Honor",
	10	=> "Badge of Justice",
	11	=> "Emblem of Heroism",
	12	=> "Emblem of Valor",
	13	=> "Emblem of Conquest",
	14	=> "Sunmote",
);
// Bulgarian
$reward_texts[1] = $reward_texts[0];
$reward_texts[1][1] = "35 злато";
$reward_texts[1][2] = "100 злато";
$reward_texts[1][3] = "450 злато";
$reward_texts[1][4] = "1000 злато";
// German
$reward_texts[2] = array(
	1	=> "35 Gold",
	2	=> "100 Gold",
	3	=> "450 Gold",
	4	=> "1000 Gold",
	5	=> "Ehrenabzeichen der Kriegshymnenschlucht",
	6	=> "Ehrenabzeichen des Arathibeckens",
	7	=> "Ehrenabzeichen des Alteractals",
	8	=> "Ehrenabzeichen vom Auge des Sturms",
	9	=> "Ehrenabzeichen von Tausendwinter",
	10	=> "Abzeichen der Gerechtigkeit",
	11	=> "Emblem des Heldentums",
	12	=> "Emblem der Ehre",
	13	=> "Emblem der Eroberung",
	14	=> "Sonnenpartikel",
);
// Spanish
$reward_texts[3] = array(
	1	=> "35 oro",
	2	=> "100 oro",
	3	=> "450 oro",
	4	=> "1000 oro",
	5	=> "Marca de Honor de la Garganta Grito de Guerra",
	6	=> "Marca de Honor de la Cuenca de Arathi",
	7	=> "Marca de Honor del Valle de Alterac",
	8	=> "Marca de Honor del Ojo de la Tormenta",
	9	=> "Marca de Honor de Conquista del Invierno",
	10	=> "Distintivo de justicia",
	11	=> "Emblema de heroísmo",
	12	=> "Emblema de valor",
	13	=> "Emblema de conquista",
	14	=> "Mota de sol",
);
// Portuguese
$reward_texts[4] = $reward_texts[0];
$reward_texts[4][1] = "35 ouro";
$reward_texts[4][2] = "100 ouro";
$reward_texts[4][3] = "450 ouro";
$reward_texts[4][4] = "1000 ouro";
// Swedish
$reward_texts[5] = $reward_texts[0];
$reward_texts[5][1] = "35 guld";
$reward_texts[5][2] = "100 guld";
$reward_texts[5][3] = "450 guld";
$reward_texts[5][4] = "1000 guld";
// French
$reward_texts[6] = array(
	1	=> "35 or",
	2	=> "100 or",
	3	=> "450 or",
	4	=> "1000 or",
	5	=> "Marque d'honneur du goulet des Chanteguerres",
	6	=> "Marque d'honneur du bassin d'Arathi",
	7	=> "Marque d'honneur de la vallée d'Alterac",
	8	=> "Marque d'honneur de l'Oeil du cyclone",
	9	=> "Marque d'honneur de Joug-d'hiver",
	10	=> "Insigne de justice",
	11	=> "Emblème d'héroïsme",
	12	=> "Emblème de vaillance",
	13	=> "Emblème de conquête",
	14	=> "Granule de soleil",
);
// Russian
$reward_texts[7] = array(
	1	=> "35 золото",
	2	=> "100 золото",
	3	=> "450 золото",
	4	=> "1000 золото",
	5	=> "Почетный знак Ущелья Песни Войны",
	6	=> "Почетный знак Низины Арати",
	7	=> "Почетный знак Альтеракской долины",
	8	=> "Почетный знак Ока Бури",
	9	=> "Почетный знак Озера Ледяных Оков",
	10	=> "Знак справедливости",
	11	=> "Эмблема героизма",
	12	=> "Эмблема доблести",
	13	=> "Эмблема завоевания",
	14	=> "Частица солнца",
);
?>